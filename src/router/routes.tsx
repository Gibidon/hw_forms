import { App } from '../App'
import { Signin } from '../components/Signin'
import { Signup } from '../components/Signup'

interface IRoute {
  [route: string]: React.ComponentType
}

export const routes: IRoute = { '/': App, '/signin': Signin, '/signup': Signup }
