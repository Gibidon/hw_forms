import { useState } from 'react'
import { InputProps } from '../components/Input'

const defaultProps: InputProps = {
  type: 'text',
  label: '',
  placeholder: '',
  description: '',
  error: '',
  radius: 'xs',
  size: 14,
  withAsterisk: false,
  icon: null,
  email: '',
  password: '',
  gender: 'male',
  className: 'border-2 rounded px-3 py-1',
}

export function useInputSettings(customSettings: InputProps) {
  const [settings, setSettings] = useState({ ...defaultProps, ...customSettings })

  return settings
}
