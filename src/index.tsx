import { createRoot } from 'react-dom/client'
import { App } from './App'
import { Route, RouterProvider, createBrowserRouter } from 'react-router-dom'
import { Signin } from './components/Signin'
import { Signup } from './components/Signup'

import { InputProps } from './components/Input'
import './index.css'

const root = document.querySelector('#root')

if (!root) {
  throw new Error('Root not found!')
}

const container = createRoot(root)

const submitHandler = (formData: InputProps) => {
  console.log(formData)
}

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      { path: '/signin', element: <Signin onSubmit={submitHandler} /> },
      { path: '/signup', element: <Signup onSubmit={submitHandler} /> },
    ],
  },
])

// export const Router = () => {
//   const mappedRoutes = Object.entries(routes).map(([path, Component]) => {
//     return (
//       <Route key={path} path={path}>
//         <Component />
//       </Route>
//     )
//   })
// }

container.render(<RouterProvider router={router} />)
