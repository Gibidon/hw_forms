import { Link } from 'react-router-dom'
import { FormEvent } from 'react'
import { Outlet } from 'react-router-dom'

export function App() {
  return (
    <div>
      <h1>Go to:</h1>
      <div>
        <Link to="/signin" className="underline text-blue-400">
          Signin page
        </Link>
        <br />
        <Link to="/signup" className="underline text-blue-400">
          Signup page
        </Link>
      </div>
      <Outlet />
    </div>
  )
}
