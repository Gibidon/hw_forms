import { InputHTMLAttributes } from 'react'
import { useInputSettings } from '../hooks/useInputSettings'

enum Radiuses {
  'xs' = 'rounded',
  'sm' = 'rounded-sm',
  'md' = 'rounded-md',
  'lg' = 'rounded-lg',
  'xl' = 'rounded-xl',
}

export interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string
  description?: string
  nickname?: string
  error?: string
  variant?: string
  radius?: 'xs' | 'sm' | 'md' | 'lg' | 'xl'
  size?: number
  disabled?: boolean
  withAsterisk?: boolean
  icon?: React.ReactNode
  email?: string
  password?: string
  passwordConfirmation?: string
  gender?: 'male' | 'female'
}

const defaultProps: InputProps = {
  type: 'text',
  label: '',
  placeholder: '',
  description: '',
  error: '',
  radius: 'xs',
  size: 18,
  withAsterisk: false,
  icon: null,
  email: '',
  password: '',

  className: 'border rounded w-full px-2 py-1',
}

export function Input(props: InputProps) {
  const {
    value,
    name,
    type,
    placeholder,
    description,
    className,
    onChange,
    error,
    label,
    icon,
    size,
    radius,
    disabled,
    withAsterisk,
    ...rest
  } = useInputSettings(props)

  return (
    <div style={{ fontSize: size + 'px' }} className="max-w-96">
      <label htmlFor={name}>
        {label} {withAsterisk && '*'}
      </label>
      {description && (
        <div style={{ fontSize: Math.round(size * 10) / 10 + 'px', color: 'lightgrey' }}>
          {description}
        </div>
      )}

      <div className="relative flex items-center">
        {icon && <span className="absolute ml-1 pointer-events-none">{icon}</span>}
        <input
          type={type}
          name={name}
          onChange={onChange}
          disabled={disabled}
          placeholder={placeholder}
          className={`w-full p-2 border ${Radiuses[radius]} ${error && 'text-red-600 outline-red-600'} ${icon && 'pl-7'}`}
        />
      </div>
    </div>
  )
}
