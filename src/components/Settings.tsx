import { Input } from './Input'

export function Settings() {
  return (
    <>
      <Input
        name="name_placeholder"
        label="Placeholder"
        placeholder="Your name"
        className="border rounded p-2 w-full"
      />
      <Input
        name="name_label"
        label="Label"
        placeholder="Full name"
        className="border rounded p-2 w-full"
      />
      <Input label="Description" placeholder="Description" className="border rounded p-2 w-full" />
      <Input label="Error" placeholder="Error" className="border rounded p-2 w-full" />
      <label htmlFor="themeSelect">Variant</label>
      <select className="border rounded p-2 w-full bg-white">
        <option>Default</option>
        <option>Dark</option>
        <option>Orange</option>
      </select>
      {/* <Input type="range" label="Radius" className="w-full" min="1" max="5" step="1" /> */}
      <Input
        type="range"
        label="Radius"
        className="w-full"
        min="0"
        max="100"
        step="20"
        list="radiusValues"
      />
      {/* <input type="range" id="temp" name="temp" list="markers" /> */}

      <datalist id="radiusValues">
        <option value="20" className="bg-white-200 p-2 cursor-pointer">
          20 percent
        </option>
        <option value="40"></option>
        <option value="60"></option>
        <option value="80"></option>
        <option value="100"></option>
      </datalist>
      <Input type="range" label="Size" className="w-full" min={1} max={5} step={1} value={1} />
      <div className="flex items-center gap-3">
        <Input type="checkbox" id="disabled" name="disabled" />
        <label htmlFor="disabled">Disabled</label>
      </div>
      <label className="inline-flex items-center cursor-pointer">
        <Input type="checkbox" value="" className="sr-only peer" checked />
        <div className="relative w-11 h-6 bg-gray-200 rounded-full peer peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-5 after:w-5 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
        <span className="ms-3 text-sm font-medium text-gray-900 dark:text-gray-300">
          With asterisk
        </span>
      </label>
    </>
  )
}
