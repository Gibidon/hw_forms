import { useState, FormEvent, ChangeEvent, FormEventHandler } from 'react'
import { Input } from './Input'
import { InputProps } from './Input'
import { Link } from 'react-router-dom'
import { FiAtSign } from 'react-icons/fi'

interface SignupProps {
  onSubmit: (formData: InputProps) => void
}

export function Signup({ onSubmit }: SignupProps) {
  const [formData, setFormData] = useState<InputProps>({
    name: '',
    nickname: '',
    email: '',
    gender: 'male',
    password: '',
    passwordConfirmation: '',
  })

  console.log(formData)

  const handleChange: FormEventHandler = (e: ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    setFormData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }))
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    onSubmit(formData)
  }

  return (
    <div className="p-4 border m-auto max-w-60 rounded bg-slate-100">
      <h1 className="font-bold text-2xl">Signup form:</h1>
      <form onSubmit={handleSubmit} className="flex flex-col gap-4">
        <Input
          type="text"
          name="name"
          label="Name"
          value={formData.value}
          onChange={handleChange}
          description="name"
          radius="sm"
        />

        <Input
          type="text"
          name="nickname"
          label="Nickname"
          value={formData.nickname}
          icon={<FiAtSign size="20px" />}
          withAsterisk={true}
          description="nickname"
          onChange={handleChange}
          radius="lg"
        />

        <Input
          type="email"
          name="email"
          label="Email"
          value={formData.email}
          onChange={handleChange}
        />
        <div className="flex gap-2">
          <Input
            type="radio"
            id="male"
            onChange={() => setFormData((prevState) => ({ ...prevState, gender: 'male' }))}
            name="gender"
          />
          <label htmlFor="male">Male</label>

          <Input
            type="radio"
            id="female"
            onChange={() => setFormData((prevState) => ({ ...prevState, gender: 'female' }))}
            name="gender"
          />
          <label htmlFor="female">Female</label>
        </div>

        <Input
          type="password"
          name="password"
          label="Password"
          value={formData.password}
          onChange={handleChange}
          error={'some error'}
        />

        <Input
          type="password"
          name="passwordConfirmation"
          label="Confirm password"
          value={formData.passwordConfirmation}
          onChange={handleChange}
          radius="xl"
        />

        <button type="submit" className="border rounded bg-orange-300 px-3 py-2 w-full">
          Зарегистрироваться
        </button>
      </form>
      <br />
      <Link to="/signin" className="text-blue-600">
        Don't have account yet? Sign in
      </Link>
    </div>
  )
}
