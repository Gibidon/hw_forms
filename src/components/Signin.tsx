import { Link } from 'react-router-dom'
import { ChangeEvent, FormEvent, FormEventHandler, useState } from 'react'
import { Input } from './Input'
import { InputProps } from './Input'

interface SigninProps {
  onSubmit: (data: InputProps) => void
}

export function Signin({ onSubmit }: SigninProps) {
  const [data, setData] = useState<InputProps>({ email: '', password: '' })

  // const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
  //   setData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }))
  // }
  const handleChange: FormEventHandler = (e: ChangeEvent<HTMLInputElement>) => {
    setData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }))
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()
    onSubmit(data)
  }

  return (
    <div className="p-4 border m-auto max-w-72 bg-zinc-100 rounded ">
      <h1 className="font-bold text-2xl m-auto max-w-36">Signin form:</h1>
      <form
        onSubmit={handleSubmit}
        onChange={handleChange}
        className="flex flex-col gap-4 items-start"
      >
        <Input type="email" name="email" value={data.email} label="Email" />
        <Input type="password" name="password" value={data.password} label="Password" />
        <button type="submit" className="bg-orange-300 border px-2 py-1 rounded text-lg w-full">
          Войти
        </button>
      </form>
      <br />
      <Link to="/signup" className="text-blue-600">
        Already have an account? Sign in
      </Link>
    </div>
  )
}
